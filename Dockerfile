FROM rocker/geospatial:4.0.3

RUN apt-get update && apt-get install -y --no-install-recommends \
		libudunits2-dev qpdf libmagick++-dev lbzip2 libgdal-dev

RUN R -e "devtools::install_gitlab('actionable-phenology/tempo')"

RUN install2.r --error \
		lmom \
		PearsonDS \
		moments \
		rnpn \
		zoo \
		nimble \
		hexSticker \
		showtext

RUN Rscript -e "update.packages(ask = FALSE)"
