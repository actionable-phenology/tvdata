#' Format covariates for tvgeomMCMC
#'
#' Function for formatting a long form covariate data frame into a suitable format stuiable for 
#' tvgeomMCMC.
#'
#' The function extracts specified covariates (\code{vars}) from a long format
#' data frame (\code{x}) for a dataset (\code{y}) for use in tvgeomMCMC. Returns a 
#' 3D array with named dimensions. Rows are named using the \code{y$obs_id}, 
#' columns are named using the time steps in \code{x$time_step},
#' and the third dimension is named for the covariates using the \code{vars} argument.
#'
#' @param x data.frame; Long form covariate data containing covariate info for every 
#' observation/sample unit at every time step. Must contain a column, 
#' \code{time_step}, containing an integer representation of the time 
#' step (e.g. year, DOY, month, minute, etc.) for each covariate observation.
#' 
#' @param y data.frame; The observation data containing a column called \code{obs_id} 
#' with unique observation IDs for each row, as well as column(s) shared 
#' with \code{x} that can be used to join covariate data to observation 
#' data. This shared column may be \code{obs_id} or another combination 
#' of different columns, e.g. \code{site_id} and \code{year} in the case that
#' a given site may have multiple observations over different years.
#' 
#' @param vars vector; a character vector with the column names of the covariates
#' to be extracted and formatted from \code{x}
#' 
#' @param join_by vector; a character vector of column names in \code{x} and \code{y}
#' that define the unique observations in y. This argument defines how to
#' join covariate data back to the observations.
#' 
#' @param n_time_steps The number of time steps you want to include in the model.
#' This argument is useful if you have covariate data for every day of the year 
#' (365 time steps), but the event can only occur in the first \emph{n} days of the year.
#' In this case, you would specify \code{n_time_steps} as \emph{n}
#' Defaults to the total number of time steps available in \code{x}, 
#' \code{length(unique(x$time_step))}
#' 
#' @importFrom dplyr "one_of"
#' @importFrom tidyselect all_of
#' @name format_covariates
NULL
#' @keywords asdm, phenology, climate, covariates
#' @examples
#' 1+1
#' # event_cases <- c("f", "F", "+")
#' # file_path <- "data-raw/asdm-phenology-jan-2017.csv"
#' # sp <- "Fouquieria splendens"
#' # source("data-raw/format_asdm.R")
#' # y <- format_asdm(file_path, event_cases, sp) 
#' # 
#' # vars <- c("dayl", "tmin", "tmax")
#' # 
#' # x <- read_csv("data/ASDM/DAYMET_azdesert_v1_long_2017-12-31.csv")  %>%
#' #   mutate(year = year(img_date),
#' #          time_step = yday(img_date)) %>% 
#' #   rename(date = img_date, site = location)
#' # 
#' # 
#' # join_by = c("site", "year")
#' # out <- format_covariates(x, y, vars, join_by = join_by)
#' # dimnames(out) 

#' @rdname format_covariates
#' @export

format_covariates <- function(x, y, vars, join_by, n_time_steps = length(unique(x$time_step))) {
  covs <- x %>%
    select(all_of(join_by), "time_step", all_of(vars))
  
  cov_array <- array(dim = c(nrow(y), n_time_steps, length(vars)),
                     dimnames = list(y$obs_id,
                                     sort(unique(covs$time_step))[1:n_time_steps],
                                     vars))
  if(!(nrow(y) > 1)) {
    stop("data set contains fewer than 2 observatoins. This function only works if n > 1")
  }
  for (i in 1:length(vars)) {
    leave_out <- vars[-i]
    all <- (covs %>% 
              select(-one_of(leave_out)) %>% 
              spread("time_step", vars[i]))
    
    cov_array[, , i] <- as.matrix(
      left_join(y,
                all,
                join_by))[, 
                          (ncol(y) + 1):(n_time_steps + ncol(y))]
  }
  
  dim_names <- list(y$obs_id,
                    sort(unique(covs$time_step))[1:n_time_steps])
                    
  cov_list <- lapply(seq_len(dim(cov_array)[[3]]),
                     FUN = function(x) {
                       out <- cov_array[, , x]
                       dimnames(out) <- dim_names
                       class(out) <- "numeric"
                       return(out)
                     })

  names(cov_list) <- vars
  
  cov_list
}

# prcp <- read_csv("data/ASDM/DAYMET_azdesert_v1_long_2017-12-31.csv") %>%
#   mutate(Year = as.numeric(format(img_date,"%Y")),
#          DOY = as.numeric(format(img_date, "%j"))) %>% 
#   select(location, Year, DOY, prcp) %>% 
#   mutate(
#     watered = case_when(location == "Museum" ~ TRUE,
#                         location == "Canyon" ~ FALSE)
#   ) %>% 
#   spread(DOY, prcp)
# prcp_in <- as.list(as.data.frame(t(as.matrix(left_join(y, prcp, by = c("year" = "Year","watered"))[,6:370]))))
# prcp15 <- t(sapply(X = prcp_in, FUN = function(input){
#   blank <- rep(0,length(input))
#   for (i in 1:length(input)){
#     blank[i] <- sum(input[max(1,i-15):i])
#   }
#   return(blank)
# }))
# 
# source('functions/getGDD.R')
# 
# #### Join covariates back to observations by year
# 
# tmin1 =left_join(y, tmin, by = c("year" = "Year","watered"))
# library(abind)
# x <- abind(
#   matrix(scale(c(as.matrix(left_join(y, tmin, by = c("year" = "Year","watered"))[,6:370]))), nrow = nrow(y), ncol = 365),
#   matrix(scale(c(as.matrix(left_join(y, tmax, by = c("year" = "Year","watered"))[,6:370]))), nrow = nrow(y), ncol = 365),
#   matrix(scale(c(prcp15)), nrow = nrow(y), ncol = 365),
#   matrix(scale(c(as.matrix(left_join(y, dayl, by = c("year" = "Year","watered"))[,6:370]))), nrow = nrow(y), ncol = 365),
#   along = 3
# )
# source('functions/getGDD.R')
# AGDD <- as.matrix(t(mapply(FUN = getGDD,
#                            tmin = as.list(as.data.frame(t(as.matrix(left_join(y, tmin, by = c("year" = "Year","watered"))[,6:370])))), 
#                            tmax = as.list(as.data.frame(t(as.matrix(left_join(y, tmax, by = c("year" = "Year","watered"))[,6:370])))), 
#                            cutoff = 5)))
# AGDD <- (AGDD - mean(AGDD)) / sd(AGDD)
# 
