#! /bin/bash
mkdir temp
cp Dockerfile temp/Dockerfile
cd temp
docker build -t tvdata-env .
cd ..
rm -rf temp/