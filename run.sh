#!/usr/bin/env bash

rm -rf .rstudio/
docker run -it --rm \
    -v "$(pwd)":/home/rstudio \
    -e PASSWORD=tvdata \
    -e ROOT=TRUE \
    -p 8787:8787 \
    tvdata_image
rm -rf kitematic/
